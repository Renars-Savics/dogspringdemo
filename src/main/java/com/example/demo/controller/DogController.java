package com.example.demo.controller;

import com.example.demo.model.Dog;
import com.example.demo.repository.DogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/dogs")
public class DogController {

    private final DogRepository dogRepository;

    @GetMapping("/all")
    public List<Dog> getAll(){
        return dogRepository.findAll();
    }
    @PostMapping("/save")
    public String saveName(@RequestBody Dog dog) {
        dogRepository.save(dog);
        return "Saved " + dog.getName();
    }

}
